package com.demo.ssm.dao;

import java.util.List;

import com.demo.ssm.pojo.Monitor;

public interface MonitorMapper {
    int deleteByPrimaryKey(Long id);

    int insert(Monitor record);

    int insertSelective(Monitor record);

    Monitor selectByPrimaryKey(Long id);
    
    List<Monitor> selectByIp(String ip);

    int updateByPrimaryKeySelective(Monitor record);

    int updateByPrimaryKey(Monitor record);
}