package com.demo.ssm.dao;

import com.demo.ssm.pojo.monitortime;

public interface monitortimeMapper {
    int deleteByPrimaryKey(Integer id);

    int insert(monitortime record);

    int insertSelective(monitortime record);

    monitortime selectByPrimaryKey(Integer id);

    int updateByPrimaryKeySelective(monitortime record);

    int updateByPrimaryKey(monitortime record);
}