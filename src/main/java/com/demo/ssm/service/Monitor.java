package com.demo.ssm.service;

public class Monitor {
	private Long id;
	private String ip;
	private String time;
	private float cpu;
	private float mem;
	
	public Monitor(Long id, String ip,String time, float cpu, float mem) {
		super();
		this.id = id;
		this.ip = ip;
		this.time = time;
		this.cpu = cpu;
		this.mem = mem;
	}
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public String getIp() {
		return ip;
	}
	public void setIp(String ip) {
		this.ip = ip;
	}
	public String getTime() {
		return time;
	}
	public void setTime(String time) {
		this.time = time;
	}
	public float getCpu() {
		return cpu;
	}
	public void setCpu(float cpu) {
		this.cpu = cpu;
	}
	public float getMem() {
		return mem;
	}
	public void setMem(float mem) {
		this.mem = mem;
	}
	
}
