package com.demo.ssm.service;

import com.demo.ssm.pojo.User;

public interface IUserService {  
    public User getUserById(Long userId);  
}  
