package com.demo.ssm.service;

import java.util.List;

import com.demo.ssm.job.SysParam;
import com.demo.ssm.pojo.Monitor;
import com.demo.ssm.pojo.User;

public interface IMonitorService {  
    int insert(Monitor record); 
    
    List<Monitor> selectByIp(String ip);
}   

