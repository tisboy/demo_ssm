package com.demo.ssm.service.impl;

import javax.annotation.Resource;

import com.demo.ssm.dao.MonitorMapper;
import com.demo.ssm.dao.monitortimeMapper;
import com.demo.ssm.job.SysParam;
import com.demo.ssm.pojo.monitortime;
import com.demo.ssm.service.IMonitorTimeService;

public class SysParamServiceImpl implements IMonitorTimeService{
	@Resource  
    private monitortimeMapper monitorTimeDao;
	
	public monitortime getNameByKey(int key){
		
		return monitorTimeDao.selectByPrimaryKey(key);
	}
}
