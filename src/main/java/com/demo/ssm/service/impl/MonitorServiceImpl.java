package com.demo.ssm.service.impl;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.demo.ssm.dao.MonitorMapper;
import com.demo.ssm.job.SysParam;
import com.demo.ssm.pojo.Monitor;
import com.demo.ssm.pojo.User;
import com.demo.ssm.service.IMonitorService;
import com.demo.ssm.service.IUserService;

@Service("monitorService")
public class MonitorServiceImpl implements IMonitorService {  
    @Resource  
    private MonitorMapper monitorDao;

	@Override
	public int insert(Monitor record) {
		return monitorDao.insert(record);
	}

	@Override
	public List<Monitor> selectByIp(String ip) {
		return monitorDao.selectByIp(ip);
	}
}  