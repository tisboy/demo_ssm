package com.demo.ssm.service.impl;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.demo.ssm.dao.UserMapper;
import com.demo.ssm.pojo.User;
import com.demo.ssm.service.IUserService;

@Service("userService")
public class UserServiceImpl implements IUserService {  
    @Resource  
    private UserMapper userDao;  
    @Override  
    public User getUserById(Long userId) {  
        return this.userDao.selectByPrimaryKey(userId);  
    }  
  
}  