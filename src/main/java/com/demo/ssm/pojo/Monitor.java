package com.demo.ssm.pojo;

import java.util.Date;

import com.demo.ssm.utils.SwitchHelper;

public class Monitor {
    private Long id;

    private Date crtime;

    private String ip;

    private Float cpu;

    private Float mem;

    private Float disk;

    private Float load1;

    private Float load5;

    private Float load15;

    private Float iow;

    private Float ior;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCrtime() {
    	return SwitchHelper.dateToString(crtime);
    }

    public void setCrtime(Date crtime) {
        this.crtime = crtime;
    }

    public String getIp() {
        return ip;
    }

    public void setIp(String ip) {
        this.ip = ip == null ? null : ip.trim();
    }
    public Float getCpu() {
        return cpu;
    }

    public void setCpu(long user,long nice,long sys,long idle,long wait,long inter,long irq) {
    	this.cpu = 100*(1-(float)idle/(float)(user+nice+sys+idle+wait+inter+irq));
    }

    public Float getMem() {
        return mem;
    }

    public void setMem(long mem,long real) {
        this.mem = 100*(1-(float)real/(float)mem);
    }

    public Float getDisk() {
        return disk;
    }

    public void setDisk(String disk) {
        this.disk = Float.parseFloat(disk);
    }

    public Float getLoad1() {
        return load1;
    }

    public void setLoad1(String load1) {
        this.load1 = Float.parseFloat(load1);
    }

    public Float getLoad5() {
        return load5;
    }

    public void setLoad5(String load5) {
        this.load5 = Float.parseFloat(load5);
    }

    public Float getLoad15() {
        return load15;
    }

    public void setLoad15(String load15) {
        this.load15 = Float.parseFloat(load15);
    }

    public Float getIow() {
        return iow;
    }

    public void setIow(long w1,long w2,long w3,long w4,String time) {
    	float ostime = SwitchHelper.timeSwitch(time);
    	float total = (float)(w1+w2+w3+w4)/1024;
        this.iow = total/(float)ostime;
    }

    public Float getIor() {
        return ior;
    }

    public void setIor(long r1,long r2,long r3,long r4,String time) {
    	float ostime = SwitchHelper.timeSwitch(time);
    	float total = (float)(r1+r2+r3+r4)/1024;
        this.ior = total/(float)ostime;
    }
}