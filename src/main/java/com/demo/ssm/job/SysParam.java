package com.demo.ssm.job;

public class SysParam {
	private String value;
	
	public String getParamValue(){
		return value;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}
}
