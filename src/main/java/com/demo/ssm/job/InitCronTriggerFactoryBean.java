package com.demo.ssm.job;

import java.io.Serializable;

import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
import org.springframework.util.StringUtils;

import com.demo.ssm.pojo.monitortime;
import com.demo.ssm.service.impl.SysParamServiceImpl;

public class InitCronTriggerFactoryBean extends CronTriggerFactoryBean implements Serializable {
	private static final long serialVersionUID = 1L;
	private SysParamServiceImpl sysParamServiceImpl;
	private int key;

	public void setKey(int key) {
		this.key = key;
	}

	public void setSysParamServiceImpl(SysParamServiceImpl sysParamServiceImpl) {
		this.sysParamServiceImpl = sysParamServiceImpl;
		setCronExpression(getCronExpressionFromDB());
	}

	private String getCronExpressionFromDB() {
		if (StringUtils.isEmpty(key))
			return "0 0 0/1 * * ?";
		monitortime sysParam = new monitortime();
		try {
			sysParam = sysParamServiceImpl.getNameByKey(key);
		} catch (Exception e) {
			e.printStackTrace();
		}
		if (sysParam != null && !StringUtils.isEmpty(sysParam.getTime()))
			return sysParam.getTime();
		return "0 0 0/1 * * ?";
	}
}