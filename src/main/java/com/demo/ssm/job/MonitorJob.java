package com.demo.ssm.job;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import org.snmp4j.smi.Variable;
import org.springframework.beans.factory.annotation.Autowired;

import com.demo.ssm.pojo.Monitor;
import com.demo.ssm.service.IMonitorService;

public class MonitorJob {
	
	private static SnmpUtil snmpUtil = new SnmpUtil();
	
	@Autowired
	private IMonitorService monitorService; 
	
	public void monitor(){
		try {
			snmpUtil.initComm("192.168.134.105");
			List<Variable> vList = snmpUtil.getPDU();
			Monitor monitor  = new Monitor();;
			if(null != vList && vList.size()>0){
				for(int i=0;i<vList.size();i++){
					monitor.setIp("192.168.134.105");
					monitor.setCrtime(new Date());
					monitor.setCpu(vList.get(0).toLong(), vList.get(1).toLong(),
							vList.get(2).toLong(), vList.get(3).toLong(), vList.get(4).toLong(),
							vList.get(5).toLong(), vList.get(6).toLong());
					monitor.setMem(vList.get(7).toLong(), vList.get(8).toLong());
					monitor.setDisk(vList.get(9).toString());
					monitor.setLoad1(vList.get(10).toString());
					monitor.setLoad5(vList.get(11).toString());
					monitor.setLoad15(vList.get(12).toString());
					monitor.setIow(vList.get(13).toLong(),vList.get(14).toLong(),
							vList.get(15).toLong(),vList.get(16).toLong(),vList.get(vList.size()-1).toString());
					monitor.setIor(vList.get(17).toLong(),vList.get(18).toLong(),
							vList.get(19).toLong(),vList.get(20).toLong(),vList.get(vList.size()-1).toString());
				}
				
				monitorService.insert(monitor);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}  
		
	}
}
