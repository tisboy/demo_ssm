package com.demo.ssm.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

public class Configuration {
	private static Properties properties;
	static {
		if (properties == null) {
			try {
				String filePath = System.getProperty("user.dir") + File.separator + "monitor.properties";
				File file = new File(filePath);
				properties = new Properties();
				if (file.exists()) {
					properties.load(new FileInputStream(file));
				} else {
					properties.load(Configuration.class.getResourceAsStream("/monitor.properties"));
				}
			} catch (IOException e) {
				properties = null;
				System.out.print("读取catfruit.properties配置文件出错");
			}
		}
	}
	/**
	 * 读取指定参数
	 * @param param
	 * @return
	 */
	public static String getProperty(String param){
		return properties.getProperty(param);
	}
	/**
	 * 读取monitor.txt路径
	 * @return
	 */
	public static String getDir(){
		return properties.getProperty("dir");
	}
}
