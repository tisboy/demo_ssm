package com.demo.ssm.utils;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.demo.ssm.pojo.Monitor;
import com.github.abel533.echarts.axis.AxisLine;
import com.github.abel533.echarts.axis.CategoryAxis;
import com.github.abel533.echarts.axis.SplitLine;
import com.github.abel533.echarts.axis.ValueAxis;
import com.github.abel533.echarts.code.SeriesType;
import com.github.abel533.echarts.code.Trigger;
import com.github.abel533.echarts.code.X;
import com.github.abel533.echarts.code.Y;
import com.github.abel533.echarts.json.GsonOption;
import com.github.abel533.echarts.series.Line;
import com.github.abel533.echarts.style.ControlStyle.Color;
import com.github.abel533.echarts.style.LineStyle;

public class EchartUtils {
	public static Map<String ,Object> returnMonitor(List<Monitor> ml){
		
		String text = "";
		String tip = "";
		Map<String ,Object> md = new HashMap<String ,Object>();
		String monitors[] = {"cpu","mem"};
		for(int j=0;j<2;j++){
			if(j==0){
				text = "最近15分钟cpu利用率";
				tip = "cpu利用率 ";
			}else{
				text = "最近15分钟内存利用率";
				tip = "内存利用率 ";
			}
			//创建Option对象
			GsonOption option = new GsonOption();
			//设置图表标题，并且居中显示
			option.title().text(text).x(X.center);
			option.calculable(true);
		    option.tooltip().trigger(Trigger.axis).formatter(tip+"  <br/>{b} : {c}%");
		    //设置样式
		    //option.color("#000000");
		    
		    //设置图例,居中底部显示，显示边框
			option.legend().data("时间").x(X.center).y(Y.bottom).borderWidth(1);
			//设置y轴为值轴，并且显示y轴
			option.yAxis(new ValueAxis().name(tip+"(%)")
			        .axisLine(new AxisLine().show(true).lineStyle(new LineStyle().width(1)))
			        .splitNumber(10).max(100).min(0));
			//创建类目轴，并且不显示竖着的分割线，onZero=false
			CategoryAxis categoryAxis = new CategoryAxis()
			        .splitLine(new SplitLine().show(false))
			        .axisLine(new AxisLine().onZero(false));
			//不显示表格边框，就是围着图标的方框
			option.grid().borderWidth(0);
			//创建Line数据
			Line line = new Line("时间").smooth(true);
			//根据获取的数据赋值
			for(int i=ml.size()-1;i>=0;i--){
				//增加类目，值为日期
			    categoryAxis.data(ml.get(i).getCrtime().split(" ")[1]);

			    //日期对应的数据
			    if(j==0){
			    	line.data(ml.get(i).getCpu());
				}else{
					line.data(ml.get(i).getMem());
				}
			}
			//设置x轴为类目轴
			option.xAxis(categoryAxis);

			//设置数据
			option.series(line);
			
			//日期对应的数据
		    if(j==0){
		    	md.put("cpu", option.toString());
			}else{
				md.put("mem", option.toString());
			}
		}
		//System.out.println(md.toString());
		return md;
	}
}
