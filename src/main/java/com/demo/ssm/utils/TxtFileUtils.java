package com.demo.ssm.utils;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class TxtFileUtils{
    public static List<String> readTxtFile(String filePath){
    	List<String> txt = new ArrayList<String>();
        try{
        	String encoding="UTF-8";
        	File file=new File(filePath);
            if(file.isFile()&& file.exists()){ //判断文件是否存在
               InputStreamReader read = new InputStreamReader(new FileInputStream(file),encoding);//考虑到编码格式
               BufferedReader bufferedReader = new BufferedReader(read);
               String lineTxt = null;
               while((lineTxt = bufferedReader.readLine()) != null){
            	   if(lineTxt.startsWith("#") || lineTxt.isEmpty()){
            		   continue;
            	   }
            	   txt.add(lineTxt);
               }
              read.close();
         }else{
            System.out.println("找不到指定的文件");
         }
        }catch (Exception e) {
            System.out.println("读取文件内容出错");
            e.printStackTrace();
        }
		return txt;
    }
}