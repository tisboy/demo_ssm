package com.demo.ssm.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class SwitchHelper {
	//时分秒 转换成秒
	public static float timeSwitch(String time){
		String[] str = time.split(":");
		float hour = Float.parseFloat(str[0]);
		float minute = Float.parseFloat(str[1]);
		float second = Float.parseFloat(str[2]);
		float seconds = hour*3600+minute*60+second;
		return seconds;
	}
	//日期转换成字符串
	public static String dateToString(Date date){
		
		SimpleDateFormat sdf=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");  
		String str=sdf.format(date);
		return str ;
	}
	//日期字符串转换成unix时间戳
	public static long dateStringToUnixTime(String time){
		long unixTime = 0 ;
        try {
        	 SimpleDateFormat sd = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        	 //设置时区
        	 sd.setTimeZone(TimeZone.getTimeZone("GMT+24"));
        	 unixTime =sd.parse(time).getTime();
        } catch (ParseException e) {
			e.printStackTrace();
		}
        return unixTime;
	}
}
