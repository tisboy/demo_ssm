package com.demo.ssm.controller;

import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.test.annotation.IfProfileValue;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.ssm.pojo.Monitor;
import com.demo.ssm.service.IMonitorService;
import com.demo.ssm.utils.Configuration;
import com.demo.ssm.utils.EchartUtils;
import com.demo.ssm.utils.JacksonHelper;
import com.demo.ssm.utils.TxtFileUtils;
import com.google.gson.Gson;

@Controller
public class MonitorController {
	@Autowired
	private IMonitorService monitorService;
	
	/**
	 * 
	 */
	@RequestMapping(value={"/monitor"})
	public String monitor(Model model) {
		List<String> ipL = TxtFileUtils.readTxtFile(Configuration.getDir()+"monitorip.txt");
		model.addAttribute("ipl", ipL);
		return "monitor";
	}
	/**
	 * 
	 */
	@RequestMapping(value={"/monitorinfo"},produces="text/html;charset=UTF-8")
	@ResponseBody
	public String returnMonitor(Model model,@RequestBody String ip) {
		String[] nowip = ip.split("=");
		System.out.println(nowip[1]);
		List<Monitor> ml = monitorService.selectByIp(nowip[1]);
		Map <String,Object> m = EchartUtils.returnMonitor(ml);
		System.out.println(JacksonHelper.toJSON(m));
		return JacksonHelper.toJSON(m);
	}
	
}
