package com.demo.ssm.controller;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.demo.ssm.pojo.Monitor;
import com.demo.ssm.utils.Configuration;
import com.demo.ssm.utils.EchartUtils;
import com.demo.ssm.utils.JacksonHelper;
import com.demo.ssm.utils.TxtFileUtils;
import com.google.gson.Gson;

@Controller
public class IndexController {
	/**
	 * 访问根目录首页 
	 */
	@RequestMapping(value={"/index"})
	public String index(Model model) {
		List<String> ipL = TxtFileUtils.readTxtFile(Configuration.getDir()+"monitorip.txt");
		model.addAttribute("ipl", ipL);
		return "index";
	}
}
