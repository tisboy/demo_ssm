<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Amaze UI Admin index Examples</title>
    <meta name="description" content="这是一个 index 页面">
    <meta name="keywords" content="index">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="renderer" content="webkit">
    <meta http-equiv="Cache-Control" content="no-siteapp" />
    <link rel="icon" type="image/png" href="assets/i/favicon.png">
    <link rel="apple-touch-icon-precomposed" href="assets/i/app-icon72x72@2x.png">
    <meta name="apple-mobile-web-app-title" content="Amaze UI" />
    <link rel="stylesheet" href="assets/css/amazeui.min.css" />
    <link rel="stylesheet" href="assets/css/amazeui.datatables.min.css" />
    <link rel="stylesheet" href="assets/css/app.css">
    <script src="assets/js/jquery.min.js"></script>

</head>

<body data-type="widgets">
    <script src="assets/js/theme.js"></script>
    <div class="am-g tpl-g">
        <!-- 头部 -->
        <header>
            <!-- logo -->
            <div class="am-fl tpl-header-logo">
                <a href="javascript:;"><img src="assets/img/logo.png" alt=""></a>
            </div>
            <!-- 右侧内容 -->
            <div class="tpl-header-fluid">
                <!-- 侧边切换 -->
                <div class="am-fl tpl-header-switch-button am-icon-list">
                    <span>

                </span>
                </div>
                <!-- 其它功能-->
                <div class="am-fr tpl-header-navbar">
                    <ul>
                        <!-- 欢迎语 -->
                        <li class="am-text-sm tpl-header-navbar-welcome">
                            <a href="javascript:;">欢迎你, <span>Amaze UI</span> </a>
                        </li>
                        <!-- 退出 -->
                        <li class="am-text-sm">
                            <a href="javascript:;">
                                <span class="am-icon-sign-out"></span> 退出
                            </a>
                        </li>
                    </ul>
                </div>
            </div>

        </header>
        <!-- 风格切换 -->
        <div class="tpl-skiner">
            <div class="tpl-skiner-toggle am-icon-cog">
            </div>
            <div class="tpl-skiner-content">
                <div class="tpl-skiner-content-title">
                    选择主题
                </div>
                <div class="tpl-skiner-content-bar">
                    <span class="skiner-color skiner-white" data-color="theme-white"></span>
                    <span class="skiner-color skiner-black" data-color="theme-black"></span>
                </div>
            </div>
        </div>
        <!-- 侧边导航栏 -->
        <div class="left-sidebar">
            <!-- 用户信息 -->
            <div class="tpl-sidebar-user-panel">
                <div class="tpl-user-panel-slide-toggleable">
                    <div class="tpl-user-panel-profile-picture">
                        <img src="assets/img/user04.png" alt="">
                    </div>
                    <span class="user-panel-logged-in-text">
              <i class="am-icon-circle-o am-text-success tpl-user-panel-status-icon"></i>
              禁言小张
          </span>
                    <a href="javascript:;" class="tpl-user-panel-action-link"> <span class="am-icon-pencil"></span> 账号设置</a>
                </div>
            </div>

            <!-- 菜单 -->
            <ul class="sidebar-nav">
                <li class="sidebar-nav-link">
                    <a href="javascript:;" class="sidebar-nav-sub-title active">
                        <i class="am-icon-table sidebar-nav-link-logo"></i> 监控列表
                        <span class="am-icon-chevron-down am-fr am-margin-right-sm sidebar-nav-sub-ico sidebar-nav-sub-ico-rotate"></span>
                    </a>
                    <ul class="sidebar-nav sidebar-nav-sub" style="display: block;">
                       	<c:forEach var="ip" items="${ipl}" varStatus="status">
	                        <li class="sidebar-nav-link">
								<a href="#" onclick="monitor('${ip}')">
                               		<span class="am-icon-angle-right sidebar-nav-link-logo"></span>${ip}
                            	</a>
	                        </li>
                    	</c:forEach> 
                    </ul>
                </li>
            </ul>
        </div>
      
         <!-- 内容区域 -->
        <div class="tpl-content-wrapper">
            <div class="row-content am-cf">
                <div class="row">
                    <div class="am-u-sm-12 am-u-md-12 am-u-lg-12">
                        <div class="widget am-cf">
                            <div class="widget-head am-cf">
                                <div class="widget-title  am-cf">监控数据</div>
                            </div>
                            <div class="widget-body  am-fr">
	                            <div class="am-g">
								  <div class="am-u-sm-1" id="divCpu" style="width: 450px;height: 300px"></div>
								  <div class="am-u-sm-1" id="divMem" style="width: 450px;height: 300px"></div>
								</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
     	</div>
    <script src="assets/js/amazeui.min.js"></script>
    <script src="assets/js/amazeui.datatables.min.js"></script>
    <script src="assets/js/dataTables.responsive.min.js"></script>
    <script src="assets/js/app.js"></script>
    <script src="http://echarts.baidu.com/build/dist/echarts.js"></script>
	<script type="text/javascript">
		var cpuChart;
		var memChart;
		var nowip;
		$(function(){
		    // 路径配置
		    require.config({
		        paths: {
		            echarts: 'http://echarts.baidu.com/build/dist'
		        }
		    });
		    // 使用
		    require(
		        [
		            'echarts',
		            'echarts/chart/line',       //加载需要的图形    按需加载 
		        ],
		        function (ec) {
		            // 基于准备好的dom，初始化echarts图表
		            cpuChart = ec.init(document.getElementById('divCpu'));
		            memChart = ec.init(document.getElementById('divMem'));
		        }
		    );
		});
		
		function monitortime(){
			  $.ajax({
	                type: "POST",      //data 传送数据类型。post 传递
	                contentType:'application/json;charset=utf-8',
	                dataType: 'json',  // 返回数据的数据类型json
	                url: "monitorinfo.html",  // 控制器/方法
	                cache: false,      
	                data: {"ip":nowip},  //传送的数据
	                error:function(){
	                    return;
	                },success: function (data) {
	                	//为echarts对象加载数据 
	    	            cpuChart.setOption(JSON.parse(data['cpu']));
	    	            memChart.setOption(JSON.parse(data['mem']));
	                }
	          });
		}
    	function monitor(ip){
    		nowip = ip;
    		clearInterval("monitortime()");
    	    window.setInterval("monitortime()",2000);
    	   	//清数据 
    	    cpuChart.clear();
     	    memChart.clear();
     	   	cpuChart.hideLoading();
     	  	memChart.hideLoading();
    	};
    </script>
</body>

</html>